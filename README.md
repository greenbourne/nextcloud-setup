# General information

General information of a nextcloud setup with docker and docker-compose
can be found here [https://github.com/nextcloud/docker].

# Backup data

For detailed information see [https://docs.nextcloud.com/server/19/admin_manual/maintenance/restore.html].

In general to things should always be backuped before doing anything else:

1. MariaDB:
```
 docker exec <DB_CONTAINER_ID> /usr/bin/mysqldump --single-transaction -u root \
    --password=<DB_PASSWORD> nextcloud > sql_dump_${CURRENT_DATE}.bak
```

2. Nextcloud folder
```
 tar cvfz backup_${CURRENT_DATE}.tar.gz app/
```

# Updating versions:
Only update from one major release to another, like 15->16.
Change therefore the `nextcloud` image in `docker-compose.yml`
from `nextcloud:<CURRENT_OLD_MAJOR_RELEASE_TAG` to `nextcloud:<CURRENT_OLD_MAJOR_RELEASE_TAG+1`.

# Useful hints:

In order to connect to nextcloud or mariaddb image use:

```
docker exec -it -u www-data nextcloud-app bash
docker exec -it nextcloud-mariadb bash
```

# Troubleshooting

1. Docker daemon is not running

```
service docker start
```

2. Nextcloud doesn't start due to binded port

```
 netstat -pna | grep 80
```

If **apache2** is still running:
```
service apache2 stop
```
